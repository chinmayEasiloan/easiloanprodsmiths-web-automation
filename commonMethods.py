def holdKey (keyboardKey , hold_time):
    import time, pyautogui
    start = time.time()
    while time.time() - start < hold_time:
        pyautogui.press(keyboardKey)

def openbrowser():
    import time , pyautogui
    pyautogui.click(49, 71,button='right', clicks=1)
    pyautogui.press('down')
    pyautogui.press('return')
    time.sleep(1)
    # pyautogui.click(174,208)
    # pyautogui.press('command')
    # time.sleep(1)
    pyautogui.click(230,12)
    pyautogui.press('down',presses=5)
    pyautogui.press('return')
    time.sleep(3)
    pyautogui.click(342, 60)
    time.sleep(3)
    pyautogui.hotkey('command','a')
    pyautogui.typewrite('https://elapp.prodsmiths.com/')
    time.sleep(3)
    pyautogui.press('return')
    time.sleep(8)

def openAnotherTab():
     import time , pyautogui
     pyautogui.click(270,26)
     time.sleep(1)
     pyautogui.click(274,62)
     time.sleep(1)
     pyautogui.typewrite('https://messages.google.com/web/conversations')
     time.sleep(4)
     pyautogui.press('return')
     time.sleep(5)

def fillFields(x,y,input):
    import time , pyautogui
    pyautogui.click(x,y)
    time.sleep(1)
    pyautogui.hotkey('command','a')
    time.sleep(1)
    pyautogui.press('delete')
    time.sleep(1)
    pyautogui.typewrite(f'{input}')
    time.sleep(1)

def dropDown(x,y):
    import time , pyautogui
    pyautogui.click(x,y)
    time.sleep(1)
    pyautogui.press('down')
    pyautogui.press('up')
    pyautogui.press('return')
    time.sleep(1)