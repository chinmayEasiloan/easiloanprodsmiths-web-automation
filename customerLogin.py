#! python3
import pyautogui, sys
import sys
sys.dont_write_bytecode = True
#import requests
import time
import commonMethods,otpAadhaar,grabOffer

def openChromeWindow():
    
    commonMethods.openbrowser()
    pyautogui.click(1232,156)
    time.sleep(1)
    pyautogui.click(470,442)
    time.sleep(5)
    pyautogui.typewrite('8369887988')
    time.sleep(3)
    pyautogui.click(423,507)
    time.sleep(1)

    # #Open new tab and open google web message
    commonMethods.openAnotherTab()
    pyautogui.click(169,308)
    time.sleep(5)
    pyautogui.click(169,308)
    time.sleep(5)
    pyautogui.click(861, 733,button='right', clicks= 1)
    time.sleep(3)
    pyautogui.hotkey('command', 'c')
    pyautogui.click(471,26)
    time.sleep(3)
    pyautogui.click(124,25)
    time.sleep(3)
    pyautogui.click(326,596)
    time.sleep(3)
    pyautogui.hotkey('command', 'v')
    time.sleep(3)
    pyautogui.click(414,712)
    time.sleep(1)

    
    #Loan Type and RequiredLoan Amount
    pyautogui.click(319,470)
    time.sleep(1)
    pyautogui.click(420,592)
    time.sleep(1)
    pyautogui.hotkey('command','a')
    pyautogui.press('delete')
    pyautogui.typewrite('3,80,000')
    time.sleep(1)
    
    # preffered loan
    pyautogui.click(685,591)
    time.sleep(1)
    pyautogui.hotkey('command','a')
    time.sleep(1)
    pyautogui.press('delete')
    time.sleep(1)
    pyautogui.typewrite('10')
    time.sleep(1)

    
    # Type of employment
    pyautogui.click(1010,585)
    time.sleep(3)
    pyautogui.press('down')
    pyautogui.press('up')
    pyautogui.press('return')
    time.sleep(3)

    
    #Employer type
    commonMethods.dropDown(438,681)

    # # OVERALL WORK EXPERIENCE (IN YEARS)
    commonMethods.fillFields(685,682,10)
    
    
    # # MONTHLY INCOME
    commonMethods.fillFields(1017,681,100000)
    
    # # OTHER ANNUAL INCOME
    commonMethods.fillFields(412,773,400000)

    # #EXISTING MONTHLY EMIS (SPECIFY AMOUNT)
    commonMethods.fillFields(700,769,10000)

    pyautogui.click(383,848)
    
    #e-Kyc screen
    #aadhaar
    time.sleep(12)
    commonMethods.fillFields(405,536,532135200283)
    #confirm aadhaar
    commonMethods.fillFields(670,533,532135200283)
    #check box
    pyautogui.click(282,762)
    time.sleep(10)
    #continue
    pyautogui.click(384,817)

    #optinal 
    # pyautogui.click(848,239)
    
    commonMethods.openAnotherTab()
    time.sleep(15)
    otpAadhaar.otpAadhaar()


    #kyc aadhar otp
    time.sleep(5)
    pyautogui.click(125,27)
    time.sleep(1)
    pyautogui.click(305,531)
    pyautogui.hotkey('command', 'v')
    time.sleep(3)
    commonMethods.fillFields(306,624,1234)
    pyautogui.click(383,692)
    time.sleep(12)

    #personal details
    # first name
    commonMethods.fillFields(458,453,'Chinmay')

    # Middle name
    commonMethods.fillFields(731,455,'Prakash')

    # Last name
    commonMethods.fillFields(974,455,'Gawde')

    # email 
    commonMethods.fillFields(476,544,'gawdechinmay1234@gmail.com')

    # DOB 
    commonMethods.fillFields(714,543,'08/12/1998')
    
    # Address 
    commonMethods.fillFields(939,632,'A/402 Riddhi siddhi park CHSL, near fish market Kharegaon ,Thane(w) 400605.')

    # Pinciode 
    commonMethods.fillFields(464,718,'400605')
    
    # Pan 
    commonMethods.fillFields(469,813,'DLCPG5059K')

    # aadhaar 
    commonMethods.fillFields(764,811,'532135200283')

    pyautogui.scroll(-100)
    time.sleep(1)

    pyautogui.click(383,456)
    time.sleep(1)
    pyautogui.click(651,455)
    time.sleep(1)
    pyautogui.click(517,569)
    time.sleep(1)

    # developer name 
    commonMethods.fillFields(704,545,'Lodha')

    # project name 
    commonMethods.fillFields(985,544,'NP16')

    #name of bulding 
    commonMethods.fillFields(505,684,'Excelus')

    #locality
    commonMethods.fillFields(759,686,'kalwa')

    #city
    commonMethods.fillFields(973,678,'Mumbai')

    pyautogui.click(414,823)
    time.sleep(10)

    # Grab offer
    grabOffer.grabOffer()

    #logout
    time.sleep(5)
    pyautogui.click(1244,152)

    #Logout and close tab
    # pyautogui.click(1197,146)
    # time.sleep(2)
    # pyautogui.click(231,17)
    # time.sleep(1)

# def enterOTP():
#     pyautogui.click(282, 585)
#     time.sleep(1)
#     otp1 = pyautogui.prompt("Enter Otp1 ")
#     time.sleep(1)
#     pyautogui.typewrite(otp1)
#     time.sleep(1)

#     pyautogui.click(340, 601)
#     time.sleep(4)


if __name__ == '__main__':
    openChromeWindow()